from collections import defaultdict
import pathlib as pl
import pandas as pd
import numpy as np
import pyqtgraph as pg
from qt_edges import Lines, Arcs
import pyqtgraph.exporters
from multiprocessing import Pool


def drawLine(x0, y0, x1, y1):
    tiles = set()
    steep = abs(y1 - y0) > abs(x1 - x0)
    
    if steep:
        x0, y0 = y0, x0
        x1, y1 = y1, x1

    if x0 > x1:
        y0, x1 = x1, x0
        y0, y1 = y1, y0
    
    dx = x1 - x0
    dy = y1 - y0
    if dx == 0.0:
        gradient = 1.0
    else:
        gradient = dy / dx

    xend = round(x0)
    yend = y0 + gradient * (xend - x0)

    xpxl1 = xend
    ypxl1 = int(yend)
    if steep:
        tiles.add((ypxl1,   xpxl1))
        tiles.add((ypxl1+1, xpxl1))
    else:
        tiles.add((xpxl1, ypxl1  ))
        tiles.add((xpxl1, ypxl1+1))
    intery = yend + gradient 
    
    xend = round(x1)
    yend = y1 + gradient * (xend - x1)
    xpxl2 = xend 
    ypxl2 = int(yend)
    if steep:
        tiles.add((ypxl2  , xpxl2))
        tiles.add((ypxl2+1, xpxl2))
    else:
        tiles.add((xpxl2, ypxl2))
        tiles.add((xpxl2, ypxl2+1))
    
    if steep:
        for x in range(xpxl1 + 1, xpxl2):
                tiles.add((int(intery)  , x))
                tiles.add((int(intery)+1, x))
                intery = intery + gradient
    else:
        for x in range(xpxl1 + 1, xpxl2):
                tiles.add((x, int(intery)))
                tiles.add((x, int(intery)+1))
                intery = intery + gradient
    return tiles


def get_tiles_for_edge(x_from, y_from, x_to, y_to, 
                       x_max, x_min, y_max, y_min, step):
    tiles = set()
    x0 = (x_from - x_min)/step
    y0 = (y_from - y_max)/step
    x1 = (x_to - x_min)/step
    y1 = (y_to - y_max)/step

    delta_x = 1/4
    delta_y = (y1-y0)/(x1-x0) * delta_x

    x = x0
    y = y0
    while x < x1 or y < y1:
        tiles.add((int(x), int(y)))
        x += delta_x
        y += delta_y
    print(x0,y0,x1,y0)
    print(tiles)
    return tiles


def check_tile_belonging(nodes_df, edges_df, tile_z):
    tile_range = 2**tile_z

    x_max = nodes_df['x'].max()
    x_min = nodes_df['x'].min()
    y_max = nodes_df['y'].max()
    y_min = nodes_df['y'].min()

    step = (x_max - x_min) // tile_range + 1

    nodes_by_tiles = defaultdict(set)
    edges_by_tiles = defaultdict(set)
    for node_name, node_x, node_y in \
        zip(nodes_df['name'], nodes_df['x'], nodes_df['y']):
        tile_x = int((node_x - x_min)//step)
        tile_y = int(-(node_y - y_max)//step)
        nodes_by_tiles[(tile_x, tile_y)].add(node_name)

    for edge_id, x_from, y_from, x_to, y_to in \
        zip(edges_df.index, edges_df['x_from'], edges_df['y_from'],
            edges_df['x_to'], edges_df['y_to']):
        tile_x_from = int((x_from - x_min)//step)
        tile_y_from = int(-(y_from - y_max)//step)
        edges_by_tiles[(tile_x_from, tile_y_from)].add(edge_id)
        tile_x_to = int((x_to - x_min)//step)
        tile_y_to = int(-(y_to - y_max)//step)
        edges_by_tiles[(tile_x_to, tile_y_to)].add(edge_id)
        # if tile_x_from > tile_x_to and tile_y_from != tile_y_to:
        # tiles = get_tiles_for_edge(x_from, y_from, x_to, y_to,
        #                            x_max, x_min, y_max, y_min, step)
        # print(tile_x_from, tile_y_from, tile_x_to, tile_y_to)
        # print(tiles)
        # for tile in tiles:
        #     edges_by_tiles[tile].append(edge_id)

    # for edge_id, x_to, y_to in zip(edges_df.index, edges_df['x_to'], edges_df['y_to']):
    #     tile_x = int((x_to - x_min)//step)
    #     tile_y = int(-(y_to - y_max)//step)
    #     edges_by_tiles[(tile_x, tile_y)].append(edge_id)

    return nodes_by_tiles, edges_by_tiles, step


def draw_edges_qt(arcs_for_draw, edge_mode='line', 
                  pen_width=10, rgba=(255, 0, 0, 1)):
    if edge_mode == 'line':
        data = np.array(arcs_for_draw[['x_from', 'y_from', 'x_to', 'y_to']])
        edges = Lines(data, pen_width=pen_width, rgba=rgba)
    elif edge_mode == 'arc':
        data = np.array(arcs_for_draw[['cen_x', 'cen_y', 'width',
                                       'height', 'tilt']])
        edges = Arcs(data, pen_width=pen_width, rgba=rgba)
    return edges


def draw_nodes_qt(nodes_for_draw, 
                  size=1, rgba=(255, 255, 255, 120)):
    nodes = pg.ScatterPlotItem(size=size, pen=pg.mkPen(None),
                               brush=pg.mkBrush(*rgba))
    nodes.addPoints(pos=np.array(nodes_for_draw[['x', 'y']]))
    nodes.setZValue(5)
    return nodes


def get_arc_params(nodes_df, edges_df):
    cruve_factor = 10
    arcs_df = pd.DataFrame()
    x_from = []
    x_to = []
    y_from = []
    y_to = []
    nodes_xy_dict = {}
    for name, x, y in nodes_df[['name', 'x', 'y']].values:
        nodes_xy_dict[name] = (x, y)
    for edge_from, edge_to in zip(edges_df['from'], edges_df['to']):
        xy_from = nodes_xy_dict[edge_from]
        xy_to = nodes_xy_dict[edge_to]

        x_from.append(xy_from[0])
        x_to.append(xy_to[0])
        y_from.append(xy_from[1])
        y_to.append(xy_to[1])
    arcs_df['x_from'] = x_from
    arcs_df['x_to'] = x_to
    arcs_df['y_from'] = y_from
    arcs_df['y_to'] = y_to
    arcs_df['tilt'] = np.arctan((arcs_df['y_to']-arcs_df['y_from']) /
                                (arcs_df['x_to']-arcs_df['x_from']))
    where_add_pi = (arcs_df['x_to']-arcs_df['x_from']) < 0
    arcs_df.loc[where_add_pi, 'tilt'] += np.pi
    inner_ellipse_angel = np.arctan(1/cruve_factor)
    arcs_df.loc[:, 'tilt'] += inner_ellipse_angel

    vec = [arcs_df['x_to']-arcs_df['x_from'], arcs_df['y_to']-arcs_df['y_from']]
    norm_of_vec = np.linalg.norm(vec, axis=0)
    arcs_df['width'] = np.cos(inner_ellipse_angel)*norm_of_vec*2
    arcs_df['height'] = arcs_df['width']/cruve_factor
    arcs_df['cen_x'] = np.cos(arcs_df['tilt'])*arcs_df['width']/2 + \
                                                arcs_df['x_from']  # noqa: E127
    arcs_df['cen_y'] = np.sin(arcs_df['tilt'])*arcs_df['width']/2 + \
                                                arcs_df['y_from']  # noqa: E127
    arcs_df['tilt'] = np.rad2deg(arcs_df['tilt'])
    return arcs_df


def read_gdf(gdf_file_path):
    with open(gdf_file_path) as gdf_file:
        gdf_data_lines = gdf_file.readlines()

    # node_colums = ['name', 'label', 'width', 'height', 'x', 'y', 'color']
    # edge_colums = ['node1', 'node2', 'weight', 'directed', 'color']
    nodes_df = pd.DataFrame()
    edges_df = pd.DataFrame()

    nodes_name = []
    nodes_x = []
    nodes_y = []
    edges_from = []
    edges_to = []

    edge_rows_flag = False
    for line in gdf_data_lines:
        if 'edgedef' in line:
            edge_rows_flag = True
            continue
        if 'nodedef' in line:
            continue
        if not edge_rows_flag:
            node_data = line.split(',')
            node_data = node_data[:-3] + [','.join(node_data[-3:])]
            nodes_name.append(node_data[0])
            nodes_x.append(float(node_data[4]))
            nodes_y.append(float(node_data[5]))
        else:
            edge_data = line.split(',')
            edge_data = edge_data[:-3] + [','.join(edge_data[-3:])]
            edges_from.append(edge_data[0])
            edges_to.append(edge_data[1])

    nodes_df['name'] = nodes_name
    nodes_df['x'] = nodes_x
    nodes_df['y'] = nodes_y

    edges_df['from'] = edges_from
    edges_df['to'] = edges_to

    return nodes_df, edges_df


def size_by_z(z):
    if z < 6:
        size = z
    else:
        size = z**(1.3)
    return size


def width_by_z(z):
    if z < 2:
        width = 10
    elif z < 5:
        width = 5
    else:
        width = 2
    return width


def node_rgba_b_z(z):
    if z < 3:
        rgba = (255, 255, 255, 120)
    else:
        rgba = (255, 255, 255, 255)
    return rgba


def edge_rgba_b_z(z):
    if z < 3:
        rgba = (255, 0, 0, 20)
    else:
        rgba = (255, 0, 0, 20)
    return rgba


def check_uniq_nodes(nodes_df):
    if len(nodes_df['name']) != len(set(nodes_df['name'])):
        print('Not all nodes in df is unique')
        print(len(nodes_df['name']), len(set(nodes_df['name'])))
    print('All nodes are unique')


def check_uniq_edges(edges_df):
    pairs_list = edges_df[['from', 'to']].values
    pairs_list = [(e[0], e[1]) for e in pairs_list]
    if len(pairs_list) != len(set(pairs_list)):
        print('Not all edges in df is unique')
        print(len(pairs_list), len(set(pairs_list)))
    print('All edges are unique')


def save_tile_qt(args):
    tile, z, step, x_min, y_max, view_box = args
    tile_x, tile_y = tile
    x_range = (tile_x * step + x_min, (tile_x + 1) * step + x_min)
    y_range = (-tile_y * step + y_max, -(tile_y + 1) * step + y_max)
    view_box.setRange(padding=0, disableAutoRange=True,
                      xRange=x_range, yRange=y_range)

    img_path = pl.Path(f'qt_img/{z}_{tile_x}_{tile_y}.png')
    if not img_path.exists():
        exporter = pg.exporters.ImageExporter(view_box)
        exporter.parameters()['width'] = 256
        exporter.parameters()['height'] = 256
        exporter.export(str(img_path))


def main():
    graph_file_path = pl.Path("../graphs/1e4_FA2.gdf")
    nodes_df, edges_df = read_gdf(graph_file_path)
    print('File read.')

    check_uniq_nodes(nodes_df)
    check_uniq_edges(edges_df)

    cache_path = pl.Path('./cached_arcs/' + graph_file_path.stem + '.csv')
    if cache_path.exists():
        arcs_df = pd.read_csv(cache_path)
        print('Found cached data in', str(cache_path))
    else:
        arcs_df = get_arc_params(nodes_df, edges_df)
        if not cache_path.parent.exists():
            cache_path.parent.mkdir()
        arcs_df.to_csv(cache_path)
        print('Arcs params computed and cached in', str(cache_path))

    x_max = nodes_df['x'].max()
    x_min = nodes_df['x'].min()
    y_max = nodes_df['y'].max()
    y_min = nodes_df['y'].min()

    for z in range(1, 9):
        plt = pg.plot()
        nodes = draw_nodes_qt(nodes_df, size=size_by_z(z),
                              rgba=node_rgba_b_z(z))
        plt.addItem(nodes)

        tile_range = 2**z
        step = (x_max - x_min) // tile_range + 1

        edges = draw_edges_qt(arcs_df, edge_mode='line',
                              pen_width=width_by_z(z), rgba=edge_rgba_b_z(z))
        plt.addItem(edges)

        plt.setGeometry(0, 0, 1000+37, 1000+22)
        view_box = plt.plotItem.getViewBox()
        view_box.setAspectLocked(lock=True, ratio=1)
        # plt.setAspectLocked(lock=True, ratio=1)
        view_box.setLimits(xMin=x_min, xMax=x_max, yMin=y_min, yMax=y_max)
        plt.hideAxis('bottom')
        plt.hideAxis('left')
        view_box.setDefaultPadding(0)

        # tiles_list = [((x, y), z, step, x_min, y_max, view_box)
        tiles_list = [(x, y)
                      for x in range(tile_range)
                      for y in range(tile_range)]
        # TODO: multiprocessing
        # with Pool(4) as pool:
        #     pool.map(save_tile_qt, tiles_list)
        for tile_x, tile_y in tiles_list:
            x_range = (tile_x * step + x_min, (tile_x + 1) * step + x_min)
            y_range = (-tile_y * step + y_max, -(tile_y + 1) * step + y_max)
            view_box.setRange(padding=0, disableAutoRange=True,
                              xRange=x_range, yRange=y_range)

            img_path = pl.Path(f'qt_img/{z}_{tile_x}_{tile_y}.png')
            if not img_path.exists():
                exporter = pg.exporters.ImageExporter(view_box)
                exporter.parameters()['width'] = 256
                exporter.parameters()['height'] = 256
                exporter.export(str(img_path))

        print(z, '/', 64)

    # tile_z = 2
    # nodes_by_tiles, edges_by_tiles, step = check_tile_belonging(nodes_df, arcs_df, tile_z)
    # print('Nodes and edges sorted by tiles.')

    # tiles_list = [(x, y) for x in range(2**tile_z) for y in range(2**tile_z)]
    # for i, tile in enumerate(tiles_list):
        # fig = plt.figure(frameon=False)
        # ax = plt.Axes(fig, [0., 0., 1., 1.])
        # fig.set_rasterized(True)
        # ax.set_rasterized(True)
        # ax.set_axis_off()
        # fig.add_axes(ax)
        # tile_x, tile_y = tile
        # x_min = nodes_df['x'].min()
        # y_min = nodes_df['y'].min()

        # nodes_for_draw = nodes_df[nodes_df['name'].isin(nodes_by_tiles[tile])]
        # draw_tile_nodes(nodes_for_draw)
        # plt = pg.plot()
        # nodes = draw_nodes_qt(nodes_for_draw)
        # plt.addItem(nodes)

        # tile_arcs_df = arcs_df.iloc[list(edges_by_tiles[tile])]
        # print(tile_arcs_df.iloc[0,:])
        # draw_tile_edges(tile_arcs_df)
        # edges = draw_edges_qt(tile_arcs_df, edge_mode='line')
        # plt.addItem(edges)
        # plt.setAspectLocked(lock=True, ratio=1)

        # ax.set_xlim(tile_x*step + x_min, (tile_x + 1)*step + x_min)
        # ax.set_ylim(tile_y*step + y_min, (tile_y + 1)*step + y_min)

        # ax.set_aspect(1)
        # tiles_dir_path = pl.Path('tiles')
        # if not tiles_dir_path.exists():
        #     tiles_dir_path.mkdir()
        # fig.savefig(f'tiles/{tile_z}_{tile_x}_{tile_y}.png', dpi=DPI,)
        # fig.clear()
        # plt.close()
        # plt.cla()
        # print(i + 1, '/', len(tiles_list))

    print('Done.')


if __name__ == '__main__':
    main()
    # pg.exec()
