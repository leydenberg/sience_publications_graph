import pyqtgraph as pg
from pyqtgraph import QtCore, QtGui
# import numpy as np

# Create a subclass of GraphicsObject.
# The only required methods are paint() and boundingRect() 
# (see QGraphicsItem documentation)
class Lines(pg.GraphicsObject):
    def __init__(self, data, pen_width=10, rgba=(255, 0, 0, 60)):
        pg.GraphicsObject.__init__(self)
        self.data = data  # data must have fields: time, open, close, min, max
        self.pen_width = pen_width
        self.rgba = rgba
        self.generatePicture()
    
    def generatePicture(self):
        # pre-computing a QPicture object allows paint() to run  
        # much more quickly, rather than re-drawing the shapes every time.
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        pen = QtGui.QPen()
        # pen.setWidthF(self.pen_width)
        pen.setColor(QtGui.QColor(*self.rgba))
        p.setPen(pen)
        for (x0, y0, x1, y1) in self.data:
            p.drawLine(QtCore.QPointF(x0, y0), QtCore.QPointF(x1, y1))
        p.end()
    
    def paint(self, p, *args):
        p.drawPicture(0, 0, self.picture)
    
    def boundingRect(self):
        # boundingRect _must_ indicate the entire area that will be drawn on
        # or else we will get artifacts and possibly crashing.
        # (in this case, QPicture does all the work of 
        # computing the bouning rect for us)
        return QtCore.QRectF(self.picture.boundingRect())


class Arcs(pg.GraphicsObject):
    def __init__(self, data, pen_width=10, rgba=(255, 0, 0, 60)):
        pg.GraphicsObject.__init__(self)
        self.data = data  # data must have fields: time, open, close, min, max
        self.pen_width = pen_width
        self.rgba = rgba
        self.generatePicture()
    
    def generatePicture(self):
        # pre-computing a QPicture object allows paint() to run  
        # much more quickly, rather than re-drawing the shapes every time.
        self.picture = QtGui.QPicture()
        p = QtGui.QPainter(self.picture)
        pen = QtGui.QPen()
        pen.setWidthF(self.pen_width)
        pen.setColor(QtGui.QColor(*self.rgba))
        p.setPen(pen)
        for (cen_x, cen_y, width, height, tilt) in self.data:
            rect = QtCore.QRectF(-width/2, -height/2, width, height)
            p.translate(cen_x, cen_y)
            p.rotate(tilt)
            p.drawArc(rect, 180*16, -90*16)
            p.rotate(-tilt)
            p.translate(-cen_x, -cen_y)
        p.end()
    
    def paint(self, p, *args):
        p.drawPicture(0, 0, self.picture)
    
    def boundingRect(self):
        # boundingRect _must_ indicate the entire area that will be drawn on
        # or else we will get artifacts and possibly crashing.
        # (in this case, QPicture does all the work of 
        # computing the bouning rect for us)
        return QtCore.QRectF(self.picture.boundingRect())


if __name__ == '__main__':
    # data = np.random.random((1000000,4))
    # item = Lines([[100,100,0,0], [100,0,0,0], [0,0,50,0]])
    data = [[6601.820267, 2939.268927, 2230.062672, 223.006267, 86.510747]]
    item = Arcs(data, opacity=255, rgb=(0,0,255), pen_width=5)
    nodes = pg.ScatterPlotItem(size=5, pen=pg.mkPen(None), 
                               brush=pg.mkBrush(255, 255, 255, 255))
    nodes.addPoints(pos=[[6533.958000, 1826.304600], [6713.116700, 2932.482700]])
    plt = pg.plot()
    plt.addItem(item)
    plt.addItem(nodes)
    plt.setAspectLocked(lock=True, ratio=1)
    pg.exec()
