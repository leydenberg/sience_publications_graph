import psycopg2
from time import sleep


class PostgreInterface():
    def __init__(self, **login_kwargs):
        self.login_kwargs = login_kwargs
        self.conn = psycopg2.connect(**self.login_kwargs)

    def execute_sql_query(self, sql_query):
        while self.conn:
            try:
                with self.conn.cursor() as cursor:
                    cursor.execute(sql_query)
                    records = cursor.fetchall()
                    self.conn.commit()
                return records
            except psycopg2.OperationalError:
                print("SQL server closed the connection unexpectedly. \
                       Try again in 5 sec.")
                if self.conn: 
                    self.conn.close()
            except psycopg2.InterfaceError: 
                print("Connection with SQL server already closed \
                       Try again in 5 sec.")
                if self.conn: 
                    self.conn.close()
            self.conn = None
            sleep(5)
            try:
                self.conn = psycopg2.connect(**self.login_kwargs)
            except psycopg2.OperationalError as err:
                print(err)
        # else:
        #     print('No connection with SQL server')
            # error_logging(str(sql_query))

    def close(self):
        self.conn.close()
