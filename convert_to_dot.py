from psycopg2 import sql
from datetime import datetime
from postgre_interface import PostgreInterface as psql

with open('db_login_info.dat', 'r') as info_file:
    db_login_info = info_file.read().split('\n')
    db_host = db_login_info[0]
    db_name = db_login_info[1]
    db_user = db_login_info[2]
    db_password = db_login_info[3]

psql_conn = psql(dbname=db_name, user=db_user, password=db_password,
                 host=db_host)


def error_logging(*args):
    current_time = str(datetime.now())
    with open('errors.log', 'a') as log_file:
        log_file.write(current_time + ' ')
        log_file.write(' '.join(args) + '\n')


def get_data_from_db(offset=0):
    limit = 10000
    sql_query = sql.SQL('SELECT bib, ref, cit FROM articles LIMIT {} OFFSET {};')\
                   .format(sql.Literal(limit),
                           sql.Literal(offset))
    records = psql_conn.execute_sql_query(sql_query)
    vertices = []
    input_edges = []
    output_edges = []
    for r in records:
        vertices.append(r[0])
        input_edges.append(r[1])
        output_edges.append(r[2])
    return vertices, input_edges, output_edges


def write_to_file(rows, file_path):
    with open(file_path, 'a') as graph_file:
        for r in rows:
            graph_file.write(r + ';')
            graph_file.write('\n')


def end_file(file_path):
    with open(file_path, 'a') as graph_file:
        graph_file.write('}')


def start_file(file_path):
    with open(file_path, 'w') as graph_file:
        graph_file.write('digraph scigraph {')


def convert_to_dot(vertices, input_edges, output_edges):
    rows = []
    for v, i, o in zip(vertices, input_edges, output_edges):
        rows += list(map(lambda x: x + ' -> ' + v, i))
        rows += list(map(lambda x: v + ' -> ' + x, o))
    return rows


def get_count_of_articles():
    sql_query = sql.SQL('SELECT COUNT(*) FROM articles;')
    records = psql_conn.execute_sql_query(sql_query)
    return records[0][0]


def main():
    count_of_articles = get_count_of_articles()

    file_path = 'scigraph_all.dot'
    start_file(file_path)

    for offset in range(0, count_of_articles, 10000):
        print(offset)
        vertices, input_edges, output_edges = get_data_from_db(offset)
        rows = convert_to_dot(vertices, input_edges, output_edges)
        write_to_file(rows, file_path)

    end_file(file_path)
    psql_conn.close()


if __name__ == '__main__':
    main()
