import pandas as pd
import json

gdf_file_name = "graphs/1e4_FA2.gdf"

with open(gdf_file_name) as gdf_file:
    gdf_data_lines = gdf_file.readlines()

node_colums = ['name','label','width','height','x','y','color']
edge_colums = ['node1','node2','weight','directed','color']
# df = pd.DataFrame(columns=node_colums)

nodes_list = []
edges_list = []

edge_rows_flag = False
for line in gdf_data_lines:
    if 'edgedef' in line:
        edge_rows_flag = True
        continue
    if 'nodedef' in line:
        continue
    if not edge_rows_flag:
        node_data = line.split(',')
        node_data = node_data[:-3] + [','.join(node_data[-3:])]
        node_dict = {
            "nodeid": node_data[0],
            "name": node_data[1],
            "size": 1,
            "x": node_data[4],
            "y": node_data[5],
            "community": 0
        }
        nodes_list.append(node_dict)
    else:
        edge_data = line.split(',')
        edge_data = edge_data[:-3] + [','.join(edge_data[-3:])]
        edge_dict = {
                "nodeidA": edge_data[0],
                "nodeidB": edge_data[1],
                "strength": edge_data[2]
            }
        edges_list.append(edge_dict)

with open('graphs/graph.json', 'w') as json_file:
    json_data = {
        "nodes": nodes_list,
        "relations": edges_list
    }
    json.dump(json_data, json_file)