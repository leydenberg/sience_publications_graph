import matplotlib.pyplot as plt
import pandas as pd

with open('ads_parsing.log', 'r') as log_file:
	log_data = log_file.read().split('\n')[:-1]
log_time = list(map(lambda x: ' '.join(x.split()[:2]), log_data))
log_count = list(map(lambda x: int(x.split()[2]), log_data))

data = {'time': log_time, 'count': log_count}
df = pd.DataFrame(data)

df['time'] = pd.to_datetime(df.time, format='%Y-%m-%d %H:%M:%S')

# df.plot(x='time', y='count', kind='line');
# plt.show()

incr = df['count'].max() - df['count'].min()
sec = (df['time'].max() - df['time'].min()).seconds
print(incr/sec*60*60*24*3/5000)
