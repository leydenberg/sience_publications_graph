from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import pathlib as pl
from .scripts.info_by_click import main as info_by_click


def index(request):
    return render(request, pl.Path('index.html'),)


def articles(request):
    return render(request, pl.Path('gm.html'),)


def node_info(request):
    tile_z = int(request.POST['tileZ'])
    tile_x = int(request.POST['tileX'])
    tile_y = int(request.POST['tileY'])
    x_in_tile = float(request.POST['xShift'])
    y_in_tile = float(request.POST['yShift'])
    node, node_x_in_tile, node_y_in_tile = info_by_click(tile_x, tile_y, tile_z, x_in_tile, y_in_tile)
    response = {
        'x_in_tile': node_x_in_tile,
        'y_in_tile': node_y_in_tile,
        'name': node['name'],
    }
    # return HttpResponse(str(node) + '\n' + 'https://ui.adsabs.harvard.edu/abs/' + node['name'])
    return JsonResponse(response)
