import pathlib as pl
import pandas as pd
TILE_SIZE = 256
X_MAX = 25781.25
X_MIN = -24218.75
Y_MAX = 25781.25
Y_MIN = -24218.75


def read_gdf(gdf_file_path):
    with open(gdf_file_path) as gdf_file:
        gdf_data_lines = gdf_file.readlines()

    # node_colums = ['name', 'label', 'width', 'height', 'x', 'y', 'color']
    # edge_colums = ['node1', 'node2', 'weight', 'directed', 'color']
    nodes_df = pd.DataFrame()
    edges_df = pd.DataFrame()

    nodes_name = []
    nodes_x = []
    nodes_y = []
    edges_from = []
    edges_to = []

    edge_rows_flag = False
    for line in gdf_data_lines:
        if 'edgedef' in line:
            edge_rows_flag = True
            continue
        if 'nodedef' in line:
            continue
        if not edge_rows_flag:
            node_data = line.split(',')
            node_data = node_data[:-3] + [','.join(node_data[-3:])]
            nodes_name.append(node_data[0])
            nodes_x.append(float(node_data[4]))
            nodes_y.append(float(node_data[5]))
        else:
            edge_data = line.split(',')
            edge_data = edge_data[:-3] + [','.join(edge_data[-3:])]
            edges_from.append(edge_data[0])
            edges_to.append(edge_data[1])

    nodes_df['name'] = nodes_name
    nodes_df['x'] = nodes_x
    nodes_df['y'] = nodes_y

    edges_df['from'] = edges_from
    edges_df['to'] = edges_to

    return nodes_df, edges_df



def transition_to_graph(x_in_tile, y_in_tile, tile_x, tile_y, tile_z):
    tile_range = 2**tile_z
    step = (X_MAX - X_MIN) // tile_range + 1
    x = (tile_x + x_in_tile/TILE_SIZE)*step + X_MIN
    y = -(tile_y + y_in_tile/TILE_SIZE)*step + Y_MAX
    return x, y

def transition_to_tile(x, y, tile_x, tile_y, tile_z):
    tile_range = 2**tile_z
    step = (X_MAX - X_MIN) // tile_range + 1
    x_in_tile = ((x - X_MIN)/step - tile_x)*TILE_SIZE
    y_in_tile = (-(y - Y_MAX)/step - tile_y)*TILE_SIZE
    return x_in_tile, y_in_tile


def get_dist_sqr(x, y, x_cen, y_cen):
    dist_sqr = (x - x_cen)**2 + (y - y_cen)**2
    return dist_sqr


def get_nearest_node(x_cen, y_cen, node_df):
    dist_sqr = get_dist_sqr(node_df['x'], node_df['y'], x_cen, y_cen)
    res_index = dist_sqr.idxmin()
    res = node_df.iloc[res_index, :]
    return res


def main(tile_x, tile_y, tile_z, x_in_tile, y_in_tile):
    x_in_graph, y_in_graph = transition_to_graph(x_in_tile, y_in_tile,
                                                 tile_x, tile_y, tile_z)
    node = get_nearest_node(x_in_graph, y_in_graph, nodes_df)
    node_x_in_tile, node_y_in_tile = transition_to_tile(node.x, node.y, tile_x, tile_y, tile_z)
    return node, node_x_in_tile, node_y_in_tile


graph_file_path = pl.Path("../graphs/1e4_FA2.gdf")
nodes_df, _ = read_gdf(graph_file_path)
print('GDF file read.')

if __name__ == '__main__':
    tile_x, tile_y, tile_z = 18, 112, 7
    x_in_tile, y_in_tile = 176, 49.5
    main(tile_x, tile_y, tile_z, x_in_tile, y_in_tile)
