var BASE_URL = 'http://127.0.0.1:8000';
var TILE_URL = 'http://scigraph.xyz/tiles/{z}_{x}_{y}.png';
var map;
var mapEl;
var layer;
var layerID = 'my-custom-layer';
var contentTemplate =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">{head}</h1>' +
    '<div id="bodyContent">' +
    '<a href="{link_href}" target="_blank">{link_text}</a>'
    "</div>" +
    "</div>";

window.initMap = function() {
  // Select the element with id="map".
    mapEl = document.querySelector('#map');
  
  // Create a new map.
    map = new google.maps.Map(mapEl, {
        center: new google.maps.LatLng(0.0, 0.0),
        zoom: 2
    });
  
  // Create a tile layer, configured to fetch tiles from TILE_URL.
    layer = new google.maps.ImageMapType({
        name: layerID,
        getTileUrl: function(coord, zoom) {
            // console.log(coord);
            var url = TILE_URL
            .replace('{x}', coord.x)
            .replace('{y}', coord.y)
            .replace('{z}', zoom);
            return url;
        },
        tileSize: new google.maps.Size(256, 256),
        minZoom: 1,
        maxZoom: 7
    });
  
    // Apply the new tile layer to the map.
    map.mapTypes.set(layerID, layer)
    map.setMapTypeId(layerID)

    map.addListener("rightclick", onMapRightClick);
};


function createInfoWindow(latLng, x, y) {
    contentString = contentTemplate.replace('{head}','Waiting...')
    infoWindow = new google.maps.InfoWindow({
        position: latLng,
        content: contentString,
    });
    getArticleInfo(x, y, infoWindow)
    infoWindow.open({
        anchor: null,
        map,
        shouldFocus: true,
    });
}


function onMapRightClick(mapsMouseEvent) {
    coords = mapsMouseEvent.pixel
    createInfoWindow(mapsMouseEvent.latLng, coords.x, coords.y)
}


var onLongTouch; 
var timer;
var touchDuration = 500;
var currentTouch;


function touchStart(touchEvent){
    currentTouch = touchEvent
    timer = setTimeout(() => {onLongTouch()}, touchDuration)
}


function touchMove(touchEvent) {
    // currentTouch = touchEvent
    if (timer)
        clearTimeout(timer)
}


function touchEnd(){
    if (timer)
        clearTimeout(timer)
}


onLongTouch = function() {
    touchX = currentTouch.touches[0].clientX
    touchY = currentTouch.touches[0].clientY
    infoWindowlatlng = fromPixelToLatLng(map, touchX, touchY)
    createInfoWindow(infoWindowlatlng, touchX, touchY)
};


if( screen.width <= 480 ) {     
    window.addEventListener("touchstart", touchStart, false)
    window.addEventListener("touchmove", touchMove, false)
    window.addEventListener("touchend", touchEnd, false) 
}


function getArticleInfo(clickX, clickY, infoWindow){
    srcRegexp = /(\d+)_(\d+)_(\d+)/g
    images = document.getElementsByTagName("img")
    for (var i = images.length - 1; i >= 0; i--) {
        imgRect = images[i].getBoundingClientRect()
        imgTop = imgRect.top
        imgRight = imgRect.right
        imgBottom = imgRect.bottom
        imgLeft = imgRect.left 
        if ((clickX < imgRight) & (clickX > imgLeft) & 
            (clickY > imgTop) & (clickY < imgBottom) &
            images[i].src.includes("scigraph")) {
                match = srcRegexp.exec(images[i].src)
                tileX = match[2]
                tileY = match[3]
                tileZ = match[1]
                xShift = clickX - imgLeft
                yShift = clickY - imgTop
                makeRequest('POST', BASE_URL + '/articles/node_info', `&tileZ=${tileZ}&tileX=${tileX}&tileY=${tileY}&\
xShift=${xShift}&yShift=${yShift}`, function(req){alertContents(infoWindow, xShift, yShift, req)})
                break
        }
    }
}


// document.addEventListener("mousedown", getArticleInfo);

function makeRequest(method, url, data=null, funcForResponse) {
    var httpRequest = false;

    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        httpRequest = new XMLHttpRequest();
        if (httpRequest.overrideMimeType) {
            httpRequest.overrideMimeType('text/xml');
        }
    } else if (window.ActiveXObject) { // IE
        try {
            httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) {}
        }
    }

    if (!httpRequest) {
        alert('Невозможно создать экземпляр класса XMLHTTP ');
        return false;
    }
    httpRequest.onreadystatechange = function(){funcForResponse(httpRequest);} ;
    if (method == 'GET'){ 
        httpRequest.open('GET', url, true);
        httpRequest.send(data);
    } else if (method == 'POST'){
        httpRequest.open('POST', url, true);
        httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        httpRequest.setRequestHeader('X-CSRFToken', csrftoken)
        httpRequest.send(data);
    }

}


function alertContents(infoWindow, xShift, yShift, httpRequest) {
    if (httpRequest.readyState == 4) {
        if (httpRequest.status != 200) {
            alert('С запросом возникла проблема.');
            console.log(httpRequest);
        }
        else {
            res_json = JSON.parse(httpRequest.response)
            document.getElementById("bodyContent").innerHTML = res_json['name']
            infoWindowElem = document.getElementById("content").parentNode.parentNode.parentNode.parentNode
            xShiftNew = res_json['x_in_tile'] - xShift
            yShiftNew = res_json['y_in_tile'] - yShift

            projection = map.getProjection()
            infoWindowRect = infoWindowElem.getBoundingClientRect()
            infoWindowlatlng = fromPixelToLatLng(map, infoWindowRect.left + xShiftNew, infoWindowRect.top + yShiftNew + 10)
            infoWindow.setPosition(infoWindowlatlng)
            contentString = contentTemplate
                .replace('{head}','')
                .replace('{link_href}','https://ui.adsabs.harvard.edu/abs/' + res_json['name'])
                .replace('{link_text}', res_json['name'])
            infoWindow.setContent(contentString)


        }
    }
}

// function getCookie(name) {
//     let cookieValue = null;
//     if (document.cookie && document.cookie !== '') {
//         const cookies = document.cookie.split(';');
//         for (let i = 0; i < cookies.length; i++) {
//             const cookie = cookies[i].trim();
//             // Does this cookie string begin with the name we want?
//             if (cookie.substring(0, name.length + 1) === (name + '=')) {
//                 cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//                 break;
//             }
//         }
//     }
//     return cookieValue;
// }
// const csrftoken = getCookie('csrftoken');


const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value
console.log(csrftoken)


function fromPixelToLatLng(map, x, y) {
    var projection = map.getProjection();
    var topRight = projection.fromLatLngToPoint(map.getBounds().getNorthEast());
    var bottomLeft = projection.fromLatLngToPoint(map.getBounds().getSouthWest());
    var scale = 1 << map.getZoom();
    return projection.fromPointToLatLng(new google.maps.Point(x / scale + bottomLeft.x, y / scale + topRight.y));
};

document.getElementById('close_tips_window').addEventListener('click', () => {
    document.getElementById('tips_layout').remove()
})
