# Science publication graph
## Description
The project allows you to study interaction in the academic, presenting in the form of a citation graph of scientific articles, authors or institutions.

The database was generated based on the issuance [NASA ADS](https://ui.adsabs.harvard.edu/ "NASA ADS Home")

Last version of the graph avaibale on [scigraph.xyz](http://scigraph.xyz/ "SciGraph Home")
