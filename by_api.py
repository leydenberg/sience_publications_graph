import requests
from urllib.parse import urlencode, quote_plus
from psycopg2 import sql
from itertools import cycle
import multiprocessing.dummy as mp
import threading as thr
import gc
from json.decoder import JSONDecodeError
from datetime import datetime 
from time import sleep
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError
from postgre_interface import PostgreInterface as psql

MAIN_ADS_URL = "https://api.adsabs.harvard.edu/v1/search/query?"

with open('ads_tokens.dat', 'r') as token_file:
    ads_tokens_list = token_file.read().split('\n')
    ads_tokens_cycle = cycle(ads_tokens_list)
    ads_token = ads_tokens_cycle.__next__()

with open('db_login_info.dat', 'r') as info_file:
    db_login_info = info_file.read().split('\n')
    db_host = db_login_info[0]
    db_name = db_login_info[1]
    db_user = db_login_info[2]
    db_password = db_login_info[3]

psql_conn = psql(dbname=db_name, user=db_user, password=db_password,
                 host=db_host)


def error_logging(*args):
    current_time = str(datetime.now())
    with open('errors.log', 'a') as log_file:
        log_file.write(current_time + ' ')
        log_file.write(' '.join(args) + '\n')


def get_response(url_str, ads_token):
    response = None
    while True:
        try:
            response = requests.get(MAIN_ADS_URL + url_str, headers={
                                    'Authorization': 'Bearer ' + ads_token})
            if response.status_code < 500:
                response_json = response.json()
                return response_json
            elif response.status_code == 500:
                print("""Response 500, Internal Server Error. 
                         Try again in 5 sec.""")
            elif response.status_code == 502:
                print("""Response 502, Bad Gateway Error. 
                         Try again in 5 sec.""")
            elif response.status_code == 504:
                print("""Response 504, Gateway Timeout. 
                         Try again in 5 sec.""")
            else:
                print(response.status_code)
                error_logging(str(response.status_code), url_str, ads_token)
                raise HTTPError
        except JSONDecodeError as err:
            print(url_str)
            print(response.encoding)
            print(response.content)
            print(response.text)
            raise err
        except ConnectionError:
            print("Connection Error, Try again in 5 sec.")

        if response:
            error_logging(str(response.status_code), url_str, ads_token)
        else:
            error_logging(str(response), url_str, ads_token)
        sleep(5)


def make_request(query, fields, start=0):
    global ads_token
    params = {
        'q': query,
        'fl': fields,
        'rows': '2000',
        'start': str(start),
    }

    url_str = urlencode(params, quote_via=quote_plus)

    for i in range(len(ads_tokens_list)):
        r = get_response(url_str, ads_token)
        if 'response' in r:
            resp = r['response']
            del r
            return resp
        elif 'error' in r:
            if r['error'] == 'Too many requests':
                error_logging(ads_token, 'Too many requests')
                ads_token = ads_tokens_cycle.__next__()
            else:
                raise Exception(str(r))
        else:
            raise Exception(str(r))
    else:
        raise Exception('Request limit exceeded for each token')


def _get_cit_or_ref(bibcode, query_key):
    if query_key == 'cit':
        query = "citations(bibcode:" + bibcode + ")"
    elif query_key == 'ref':
        query = "references(bibcode:" + bibcode + ")"
    fields = 'bibcode'
    resp = make_request(query, fields, start=0)
    num_found = resp['numFound']
    docs = resp['docs']
    if num_found == len(docs):
        if docs:
            return docs
    else:
        for start in range(2000, num_found+2000, 2000):
            resp = make_request(query, fields, start=start) 
            docs.extend(resp['docs'])
        return docs


def get_citations(bibcode):
    return _get_cit_or_ref(bibcode, query_key='cit')


def get_references(bibcode):
    return _get_cit_or_ref(bibcode, query_key='ref')


def get_doc_by_bib(bibcode):
    fields = 'bibcode,\
              author,\
              pub,\
              date,\
              keyword,\
              title,\
              aff,\
              citation_count,\
              abstract'
    query = "bibcode:" + bibcode
    response = make_request(query, fields)
    try:
        doc = response['docs'][0]
    except IndexError:
        print('Empty response. Query: ', query)
        error_logging(query, str(response))
    return doc


def get_initial_vertices(offset=0):
    vertices = []
    if not_in_db('2021RvMPP...5....2X'):
        vertices.append('2021RvMPP...5....2X')

    limit = 10000
    sql_query = sql.SQL("""SELECT unnest(cit) FROM (SELECT cit, pub_date, 
                           publication FROM articles WHERE bib LIKE '%MNRAS%' 
                           ORDER BY pub_date DESC) as x
                           EXCEPT SELECT bib FROM articles 
                           LIMIT {} OFFSET {};""")
    sql_query = sql_query.format(sql.Literal(limit), sql.Literal(offset))
    records = psql_conn.execute_sql_query(sql_query)
    vertices.extend([x[0] for x in records])
    return vertices


def not_in_db(bibcode):
    sql_query = sql.SQL('SELECT id FROM articles WHERE bib IN ({});').\
                    format(sql.Literal(bibcode))
    records = psql_conn.execute_sql_query(sql_query)
    return not bool(records)


def add_to_db(doc, ref_docs, cit_docs):
    if ref_docs:
        ref_bibcodes = [x['bibcode'] for x in ref_docs]
    else:
        ref_bibcodes = []
    if cit_docs:
        cit_bibcodes = [x['bibcode'] for x in cit_docs]
    else:
        cit_bibcodes = []
    values = [(
        doc['bibcode'],
        ref_bibcodes,
        cit_bibcodes,
        doc['keywords'] if 'keywords' in doc else None,
        doc['pub'] if 'pub' in doc else None,
        'abstract' in doc,
        doc['date'][:10],
        doc['abstract'] if 'abstract' in doc else None,
        doc['author'] if 'author' in doc else None,
        doc['aff'] if 'aff' in doc else None,
        doc['title'] if 'title' in doc else None,
    )]
    del doc
    del ref_docs
    del cit_docs

    prepared_values = sql.SQL(',').join(map(sql.Literal, values))
    sql_query = sql.SQL('INSERT INTO articles (bib, ref, cit, keywords, \
                            publication, haveAbstract, pub_date, abstract,\
                            author, affiliation, title) \
                            VALUES {} ON CONFLICT ON CONSTRAINT \
                            unique_article_bib DO NOTHING \
                            RETURNING *').format(prepared_values)
    records = psql_conn.execute_sql_query(sql_query)
    if not records:
        status = 1
    else:
        status = 0
    return status


def parse_vertex(vertex):
    if vertex and not_in_db(vertex):
        cit_docs = get_citations(vertex)
        ref_docs = get_references(vertex)
        doc = get_doc_by_bib(vertex)
        if doc:
            status = add_to_db(doc, ref_docs, cit_docs)
        else:
            status = 1
    else:
        status = 1
    return status


def get_count_of_articles():
    sql_query = sql.SQL('SELECT COUNT(*) FROM articles;')
    records = psql_conn.execute_sql_query(sql_query)
    return records[0][0]


def log():
    articles_count = str(get_count_of_articles())
    current_time = str(datetime.now())
    with open('ads_parsing.log', 'a') as log_file:
        log_file.write(current_time + ' ' + articles_count + '\n')


def log_interval(interval, stop_event):
    while not stop_event.wait(interval):
        log()


def main():
    try:
        stop_log_event = thr.Event()
        logger = thr.Thread(target=log_interval, args=(5, stop_log_event))
        logger.start()

        initial_vertices = get_initial_vertices()
        print('Inital vertices getted. Starting parsing...')
        while initial_vertices:
            pool = mp.Pool(10)
            pool.map(parse_vertex, initial_vertices)
            pool.close()
            pool.join()
            gc.collect()
            initial_vertices = get_initial_vertices()
    except (KeyboardInterrupt, Exception) as err:
        pool.terminate()
        raise err
    else:
        pool.close() 
    finally:
        pool.join()
        psql_conn.close()
        stop_log_event.set()
        logger.join()


def get_doc_by_bib_with_none(bibcode):
    fields = 'author,\
              pub,\
              title,\
              aff'
    query = "bibcode:" + bibcode
    response = make_request(query, fields)
    try:
        doc = response['docs'][0]
    except IndexError as err:
        error_logging(query, str(response))
        raise err
    return doc


def add_to_db_with_none(bibcode, doc):
    values = [(
        doc['pub'] if 'pub' in doc else None,
        doc['author'] if 'author' in doc else None,
        doc['aff'] if 'aff' in doc else None,
        doc['title'] if 'title' in doc else None,
    )]
    prepared_values = sql.SQL(',').join(map(sql.Literal, values))
    bibcode = sql.Literal(bibcode)
    sql_query = sql.SQL("""
        UPDATE articles SET (publication, 
        author, affiliation, title) = {v} 
        WHERE bib = {b} RETURNING *;""")
    sql_query = sql_query.format(v=prepared_values, b=bibcode)
    records = psql_conn.execute_sql_query(sql_query)
    if not records:
        status = 1
    else:
        status = 0
    return status


def parse_vertex_with_none(vertex):
    doc = get_doc_by_bib_with_none(vertex)
    status = add_to_db_with_none(vertex, doc)
    return status


def get_vertices_with_none():
    vertices = []
    limit = 10000
    sql_query = sql.SQL("""
        SELECT (bib) FROM articles WHERE 
        publication IS NULL OR title IS NULL OR
        author IS NULL OR affiliation IS NULL LIMIT {};
    """)
    sql_query = sql_query.format(sql.Literal(limit))
    records = psql_conn.execute_sql_query(sql_query)
    vertices.extend([x[0] for x in records])
    return vertices


def fill_none():
    try:
        stop_log_event = thr.Event()
        logger = thr.Thread(target=log_interval, args=(5, stop_log_event))
        logger.start()

        vertices_with_none = get_vertices_with_none()
        print('Inital vertices getted. Starting parsing...')
        while vertices_with_none:
            pool = mp.Pool(10)
            pool.map(parse_vertex_with_none, vertices_with_none)
            pool.close()
            pool.join()
            gc.collect()
            vertices_with_none = get_vertices_with_none()
    except (KeyboardInterrupt, Exception) as err:
        pool.terminate()
        raise err
    else:
        pool.close() 
    finally:
        pool.join()
        psql_conn.close()
        stop_log_event.set()
        logger.join()


if __name__ == '__main__':
    main()
    # fill_none()
